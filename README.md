# PENToolboxDesignTutorial

## How to use
Clone repository and set up submodules.
```
$ git clone git@bitbucket.org:AubertKato/pentoolboxdesigntutorial.git
$ cd pentoolboxdesigntutorial
$ git submodule init
$ git submodule update
```

Generate eclipse projects.
```
./gradlew eclipse
```

Import projects in eclipse.
File>Imports>Existing Projects into Workspace.
Import first daccad, then pentoolboxdesigntutorial.