package model.chemicals;

public class PredatorSpecies extends SequenceVertex {

	/*
	 * Convenience class to implement predators without requiring an inhibited template
	 */
	public PredatorSpecies(Integer i, double init) {
		super(i, init);
		inhib = true; //considered an inhibitor
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
