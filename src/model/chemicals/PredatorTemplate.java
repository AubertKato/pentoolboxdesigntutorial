package model.chemicals;

import model.Constants;
import model.OligoGraph;

/**
 * Implementation of a Predator species for the Predator-Prey System (Fujii and Rondelez, ACS Nano 2012).
 * Since other species (the Preys) get hybridized to Predators and get extended by polymerase,
 * the implementation relies on the Template model.
 * 
 * Note that "totalConcentration" will change over time in this case.
 * @author nauberkato
 *
 * @param <E>
 */
public class PredatorTemplate<E> extends Template<E> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static double selfHybridizationRate = 1.0;
	
	public PredatorTemplate(OligoGraph<SequenceVertex, E> parent, double totalConcentration, double stackSlowdown,
			double dangleL, double dangleR, SequenceVertex from, SequenceVertex to, SequenceVertex inhib) {
		//The "to" species is the open predator (sensitive to exo). The closed version is stored here
		super(parent, totalConcentration, 0.0, 0.0, 0.0, from, to, null);
		numberOfStates = 3; // closed, with prey input, with extended prey
		//The correct saturation impact on enzymes should be set if know.
		//Always closed as part of template, so do not set any saturation for exonuclease
		this.exoKm = 0.0;
	}

	 @Override
	 public double[] flux() {
		  double[] ans;
		  
		ans = new double[] { aloneFlux(), inputFlux(), extendedFlux() };
		  
	    return ans;
	  }
	 
	 @Override
	 public double inputSequenceFlux() {
		 double concprey = from.getConcentration();
		 double conc = to.getConcentration();
		 double debug =  Constants.Kduplex * ((Double)this.parent.K.get(from)).doubleValue()*
				 concentrationWithInput - Constants.Kduplex * concprey *
				(Constants.ratioToeholdLeft*concentrationAlone + conc);
		        //The Prey is invading when the Predator is closed
		 return debug;
	 }
	 
	 @Override
	 public double outputSequenceFlux() {
		 //Flux for the *opened* version of the predator
		 double concprey = from.getConcentration();
		 double conc = to.getConcentration();
		 double debug = ((Double)this.parent.K.get(to)).doubleValue() * Constants.Kduplex *
				(Constants.alpha*concentrationAlone) - Constants.Kduplex * conc *
				(Constants.ratioToeholdLeft*concentrationAlone + 2*conc + concprey + selfHybridizationRate);
		 
		        //The Prey is invading when the Predator is closed
		 return debug;
	 }
	 
	 @Override
	 protected double aloneFlux() {
		 //Concentration for the closed version of the predator
		 double concprey = from.getConcentration();
		 double conc = to.getConcentration();
		 double debug = Constants.Kduplex * (conc*selfHybridizationRate - ((Double)this.parent.K.get(to)).doubleValue()*
				 (Constants.alpha*concentrationAlone+2*concentrationExtended) - Constants.ratioToeholdLeft*concentrationAlone*(conc+concprey));
		 return debug;
	 }
	 
	 
	 @Override
	 protected double inputFlux() {
		//Concentration for the predator-prey duplex
		 double concprey = from.getConcentration();
		 double conc = to.getConcentration();
		 double debug = Constants.Kduplex * concprey *(conc + Constants.ratioToeholdLeft*concentrationAlone)
				 -((Double)this.parent.K.get(from)).doubleValue()*concentrationWithInput
				 -this.poly*concentrationWithInput;
		 return debug;
	 }
	 
	 @Override
	 protected double extendedFlux() {
		//Flux for the double-stranded predator
		 double conc = to.getConcentration();
		 double debug = -((Double)this.parent.K.get(to)).doubleValue() * Constants.Kduplex*concentrationExtended
				 + Constants.Kduplex * conc * (Constants.ratioToeholdLeft*concentrationAlone + conc)
				 + this.poly*concentrationWithInput;
		 
		        //The Prey is invading when the Predator is closed
		 return debug;
	 }
	 
	 @Override
	 public double[] getStates() {
		    double[] ans = new double[] { this.concentrationAlone, this.concentrationWithInput, 
		    			this.concentrationExtended};
		   
		    return ans;
		  }
	 
	 @Override
	 public void setStates(double[] values) throws InvalidConcentrationException {
		    if (values.length != numberOfStates) {
		      System.err
		        .println("Error: wrong internal values setting for template " + 
		        this);
		      System.err.println("Length: "+values.length+" should be "+numberOfStates);
		      return;
		    }
		    for (int i = 0; i < numberOfStates; i++) {
		      if ((values[i] < 0.0D) || (values[i] > this.totalConcentration+1))
		      {
		        values[i] = 0.0D;
		      }
		    }

		    this.concentrationAlone = values[0];
		    this.concentrationWithInput = values[1];
		    this.concentrationExtended = values[2];
		  }

}
