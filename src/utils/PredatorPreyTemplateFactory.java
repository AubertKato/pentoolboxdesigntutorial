package utils;

import model.Constants;
import model.OligoGraph;
import model.chemicals.PredatorSpecies;
import model.chemicals.PredatorTemplate;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;


/**
 * Class implementing the "template" classes for predator-prey systems.
 * @author nauberkato
 *
 * @param <E> the type of basic templates
 */
public class PredatorPreyTemplateFactory<E> extends DefaultTemplateFactory<E> {

	public PredatorPreyTemplateFactory(OligoGraph<SequenceVertex, E> graph) {
		super(graph);
	}

	@Override
	public Template<E> create(E e) {
		if(PredatorSpecies.class.isAssignableFrom(graph.getDest(e).getClass())) {
		double dangleL = graph.dangle?graph.getDangleL(e):Constants.baseDangleL;
		return new PredatorTemplate<E>(graph, graph.getTemplateConcentration(e), graph.getStacking(e), dangleL, dangleL,graph.getSource(e),graph.getDest(e), null);
		}
		return super.create(e);
	}
}
