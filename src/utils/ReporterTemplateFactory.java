package utils;

import model.OligoGraph;
import model.chemicals.Reporter;
import model.chemicals.ReporterIndicator;
import model.chemicals.SequenceVertex;
import model.chemicals.Template;

public class ReporterTemplateFactory<E> extends DefaultTemplateFactory<E> {

	public ReporterTemplateFactory(OligoGraph<SequenceVertex, E> graph) {
		super(graph);
	}

	@Override
	public Template<E> create(E edge) {
		if(ReporterIndicator.class.isAssignableFrom(graph.getDest(edge).getClass())) {
			return new Reporter<E>(graph, graph.getTemplateConcentration(edge) , graph.getSource(edge));
		}
		return super.create(edge);
	}

}
