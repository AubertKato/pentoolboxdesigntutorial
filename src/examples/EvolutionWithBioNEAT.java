package examples;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import reactionnetwork.Library;
import use.math.gaussian.GaussianFitnessDisplayer;
import use.math.gaussian.GaussianFitnessFunction;
import erne.Evolver;

public class EvolutionWithBioNEAT {

	public static void main(String[] args) throws InterruptedException, ExecutionException, IOException, ClassNotFoundException {
		GaussianFitnessFunction fitnessFunction = new GaussianFitnessFunction();
        erne.Constants.maxEvalTime = 3000; //Actually the current default
        erne.Constants.maxEvalClockTime = -1;
        
		Evolver evolver = new Evolver(Library.startingMath, fitnessFunction, new GaussianFitnessDisplayer());
		evolver.setGUI(true);
		evolver.evolve();
        System.out.println("Evolution completed.");
	}
	
}
