package examples;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import model.OligoGraph;
import model.chemicals.SequenceVertex;
import reactionnetwork.Connection;
import reactionnetwork.ConnectionSerializer;
import reactionnetwork.ReactionNetwork;
import reactionnetwork.ReactionNetworkDeserializer;
import utils.CodeGenerator;
import utils.GraphUtils;

public class LoadGraphExample {

	public static void main(String[] args) {
		//Create an empty graph based on the PEN toolbox v1
		//Nodes are implemented as SequenceVertex objects to store chemical details such as stabilities
		//Templates are stored as String to remain as flexible and extensible as possible
		OligoGraph<SequenceVertex,String> g = GraphUtils.initGraph();
		
		//load legacy (DACCAD) file format
		CodeGenerator.openGraph(g, null, new File("files/bistable.graph"));
		
		System.out.println("Species in the system: "+g.getVertices());
		
		//load from BioNEAT (contains extra data for evolution, json file format)
		ReactionNetwork reac = null;
		
		Gson gson = new GsonBuilder().registerTypeAdapter(ReactionNetwork.class, new ReactionNetworkDeserializer())
				.registerTypeAdapter(Connection.class, new ConnectionSerializer()).create();
		 
		try {
			BufferedReader br = new BufferedReader(new FileReader("files/bistable.json"));
			reac = gson.fromJson(br, ReactionNetwork.class);
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		
		System.out.println("Species in the system: "+reac.nodes);
		
	}

}
