package examples;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Constants;
import model.OligoGraph;
import model.OligoSystem;
import model.chemicals.SequenceVertex;
import utils.GraphUtils;
import utils.PlotExpData;

/**
 * This class demonstrates the low-level API to directly manipulate OligoGraphs by implementing an Oligator.
 * This example also shows how to call the basic simulator and output concentrations over time.
 * @author Nathanael Aubert-Kato
 *
 */
public class BasicHandmadeOligator {

	public static void main(String[] args) {
		
		//Create an empty graph based on the PEN toolbox v1
		//Nodes are implemented as SequenceVertex objects to store chemical details such as stabilities
		//Templates are stored as String to remain as flexible and extensible as possible
        OligoGraph<SequenceVertex,String> g = GraphUtils.initGraph();
        
        //Create the autocatalytic species
        SequenceVertex s1 = g.getVertexFactory().create();
        //Add it to the graph and set its sequence stability and initial concentration
		g.addSpecies(s1, 100.0);
		s1.setInitialConcentration(0.1);
		
		//Add the associated autocatalytic template
		String edge = g.getEdgeFactory().createEdge(s1, s1);
		g.addActivation(edge, s1, s1, 10.0);
		
		//Create the intermediate species and activation from s1
		SequenceVertex s2 = g.getVertexFactory().create();
		g.addSpecies(s2, 50.0);
		String edge2 = g.getEdgeFactory().createEdge(s1, s2);
		g.addActivation(edge2, s1, s2, 1.0);
		
		//Create the species inhibiting the autocatalysis of s1
		SequenceVertex i3 = g.getVertexFactory().create();
		i3.setInhib(true);
		g.addSpecies(i3, 0.1);
		//Attach the inhibitor to target template
		g.addInhibition(edge, i3);
		String edge3 = g.getEdgeFactory().createEdge(s2, i3);
		g.addActivation(edge3, s2, i3, 50.0);
		
		//Next create a basic simulator
		//This class can be extended or decorated to change the way the chemistry is modeled
		//or add new elements to the PEN toolbox. 
		OligoSystem<String> os = new OligoSystem<String>(g);
		
		//Compute the system's concentrations over time
		Constants.numberOfPoints = 3000; //Simulate for 300 min
		double[][] timeSeries = os.calculateTimeSeries(null);
		
		//Print out
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<timeSeries[0].length; i++) {
			
			for(int j = 0; j<timeSeries.length; j++) {
				sb.append(timeSeries[j][i]);
				sb.append(" ");
			}
			sb.append("\n");
		}
		
        System.out.println(sb.toString());
        //Displaying time series
        JPanel panel = PlotExpData.displayData("", timeSeries, os.getActivity(), g);
		JFrame frame = new JFrame("");
		
		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}

	
	
}
