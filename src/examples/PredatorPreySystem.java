package examples;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Constants;
import model.OligoGraph;
import model.OligoSystem;
import model.chemicals.PredatorSpecies;
import model.chemicals.PredatorTemplate;
import model.chemicals.SequenceVertex;
import utils.GraphUtils;
import utils.PlotExpData;
import utils.PredatorPreyTemplateFactory;

public class PredatorPreySystem {

	
	public static void main(String[] args) {
		//Create an empty graph based on the PEN toolbox v1
		//Nodes are implemented as SequenceVertex objects to store chemical details such as stabilities
		//Templates are stored as String to remain as flexible and extensible as possible
		OligoGraph<SequenceVertex,String> g = GraphUtils.initGraph();
		
		
		//Making the prey
		//Create the autocatalytic species
        SequenceVertex s1 = g.getVertexFactory().create();
        //Add it to the graph and set its sequence stability and initial concentration
		g.addSpecies(s1, 10.0);
		s1.setInitialConcentration(0.1);
		
		//Add the associated autocatalytic template
		String edge = g.getEdgeFactory().createEdge(s1, s1);
		g.addActivation(edge, s1, s1, 10.0);
		
		//Make the predator
		SequenceVertex sp = makePredatorSpecies(g,1.0);
		g.addSpecies(sp, 1.0);
		sp.setInitialConcentration(0.1);
		
		//Add the predator-prey reactions
		String edgepp = g.getEdgeFactory().createEdge(s1, sp);
		g.addActivation(edgepp,s1,sp,0.0);
		g.setDangleL(edgepp, 1.0);
		PredatorTemplate.selfHybridizationRate = 100.0;
		
		//Creating the simulator
		OligoSystem<String> os = new OligoSystem<String>(g,new PredatorPreyTemplateFactory<String>(g));
	
		//Compute the system's concentrations over time
		Constants.numberOfPoints = 300; //Simulate for 300 min
		double[][] timeSeries = os.calculateTimeSeries(null);

		//Print out
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<timeSeries[0].length; i++) {

			for(int j = 0; j<timeSeries.length; j++) {
				sb.append(timeSeries[j][i]);
				sb.append(" ");
			}
			sb.append("\n");
		}

		System.out.println(sb.toString());
		//Displaying time series
		JPanel panel = PlotExpData.displayData("", timeSeries, os.getActivity(), g);
		JFrame frame = new JFrame("");

		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	public static SequenceVertex makePredatorSpecies(OligoGraph<SequenceVertex,String> g, double conc) {
	        SequenceVertex newvertex = g.popAvailableVertex();
	        if (newvertex == null){
	          newvertex = new PredatorSpecies(g.getVertexCount() + 1, conc);
	        } else {
	          newvertex = new PredatorSpecies(newvertex.ID, conc);
	        }
	        return newvertex;
	}
}
