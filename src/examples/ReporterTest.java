package examples;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Constants;
import model.OligoGraph;
import model.OligoSystem;
import model.chemicals.ReporterIndicator;
import model.chemicals.SequenceVertex;
import utils.GraphUtils;
import utils.PlotExpData;
import utils.ReporterTemplateFactory;

public class ReporterTest {

	
	public static void main(String[] args) {
		//Create an empty graph based on the PEN toolbox v1
		//Nodes are implemented as SequenceVertex objects to store chemical details such as stabilities
		//Templates are stored as String to remain as flexible and extensible as possible
		OligoGraph<SequenceVertex,String> g = GraphUtils.initGraph();
		
		
		//Making the prey
		//Create the autocatalytic species
        SequenceVertex s1 = g.getVertexFactory().create();
        //Add it to the graph and set its sequence stability and initial concentration
		g.addSpecies(s1, 100.0);
		s1.setInitialConcentration(10.0);
		
		g.saturablePoly = false; //no template, ignored
		g.saturableNick = false; 
		
		
		//Creating the simulator
		OligoSystem<String> os = new OligoSystem<String>(g,new ReporterTemplateFactory<String>(g));
	
		//Compute the system's concentrations over time without reporter
		Constants.numberOfPoints = 3000; //Simulate for 300 min
		double[][] timeSeries1 = os.calculateTimeSeries(null);

		
		//add a reporter
		SequenceVertex r = new ReporterIndicator(-1);
        g.addActivation("Rs1", s1, r, 200.0); 
        
        //Make a new oligosystem
        os = new OligoSystem<String>(g,new ReporterTemplateFactory<String>(g));
        
      //Compute the system's concentrations over time without reporter
      	double[][] timeSeries2 = os.calculateTimeSeries(null);
      	
      	
      //Make a full oligator
      	g.saturablePoly = true; //no template, ignored
		g.saturableNick = true; 
		//Add the associated autocatalytic template
				String edge = g.getEdgeFactory().createEdge(s1, s1);
				g.addActivation(edge, s1, s1, 10.0);
				
				//Create the intermediate species and activation from s1
				SequenceVertex s2 = g.getVertexFactory().create();
				g.addSpecies(s2, 50.0);
				String edge2 = g.getEdgeFactory().createEdge(s1, s2);
				g.addActivation(edge2, s1, s2, 1.0);
				
				//Create the species inhibiting the autocatalysis of s1
				SequenceVertex i3 = g.getVertexFactory().create();
				i3.setInhib(true);
				g.addSpecies(i3, 0.1);
				//Attach the inhibitor to target template
				g.addInhibition(edge, i3);
				String edge3 = g.getEdgeFactory().createEdge(s2, i3);
				g.addActivation(edge3, s2, i3, 50.0);
      	
      		
      		 //Make a new oligosystem
            os = new OligoSystem<String>(g,new ReporterTemplateFactory<String>(g));
      	double[][] timeSeries3 = os.calculateTimeSeries(null);
      	
      	//change the reporter's position
      	g.removeEdge("Rs1");
      	g.addActivation("Rs2", s2, r, 200.0);
      	
      	 //Make a new oligosystem
        os = new OligoSystem<String>(g,new ReporterTemplateFactory<String>(g));
      	double[][] timeSeries4 = os.calculateTimeSeries(null);
		
		double[][] timeSeries = new double[4][];
		timeSeries[0] = timeSeries1[0];
		timeSeries[1] = timeSeries2[1];
		timeSeries[2] = timeSeries3[1];
		timeSeries[3] = timeSeries4[1];
		//Print out
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<timeSeries[0].length; i++) {

			for(int j = 0; j<timeSeries.length; j++) {
				sb.append(timeSeries[j][i]);
				sb.append(" ");
			}
			sb.append("\n");
		}

		System.out.println(sb.toString());
		//Displaying time series
		JPanel panel = PlotExpData.displayData("", timeSeries, os.getActivity(), g);
		JFrame frame = new JFrame("");

		frame.add(panel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
}
