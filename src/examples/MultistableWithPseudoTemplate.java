package examples;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Constants;
import model.OligoSystem;
import model.PseudoTemplateGraph;
import model.PseudoTemplateOligoSystem;
import model.chemicals.PseudoExtendedSequence;
import model.chemicals.Pseudotemplate;
import model.chemicals.SequenceVertex;
import model.input.PulseInput;
import utils.GraphUtils;
import utils.PlotExpData;

public class MultistableWithPseudoTemplate {

	public static void main(String[] args) {
		
		//Create an empty graph based on the PEN toolbox v1
				//Nodes are implemented as SequenceVertex objects to store chemical details such as stabilities
				//Templates are stored as String to remain as flexible and extensible as possible
				PseudoTemplateGraph<SequenceVertex,String> g = (PseudoTemplateGraph<SequenceVertex, String>) GraphUtils.initPseudoTemplateGraph();
				
				
				//Making the prey
				//Create the autocatalytic species
		        SequenceVertex s1 = g.getVertexFactory().create();
		        //Add it to the graph and set its sequence stability and initial concentration
				g.addSpecies(s1, 50.0);
				s1.setInitialConcentration(0.1);
				
				
				//Add the associated autocatalytic template
				String edge = g.getEdgeFactory().createEdge(s1, s1);
				g.addActivation(edge, s1, s1, 50.0);
				
				//Add pseudo-template
				PseudoExtendedSequence sp = new PseudoExtendedSequence(s1,0.0);
				g.setExtendedSpecies(s1, sp); //automatically had the correct templates.
				g.setTemplateConcentration(g.getPseudoTemplate(s1), 10.0);
				g.setInputSlowdown(g.findEdge(s1,s1), 0.05);
				g.setInputSlowdown(g.getPseudoTemplate(s1), 1.0);
				
				Pseudotemplate.outputInvasionRate = 0.05;
				
				//Creating the simulator
				OligoSystem<String> os = new PseudoTemplateOligoSystem(g);
				
				
				
				Constants.numberOfPoints = 1000; //Simulate for 300 min
				double[][] timeSeries1 = os.calculateTimeSeries(null);
				
				//Add a spike of 50.0 nM of s1 at time 150;
				s1.inputs.add(new PulseInput(150,50.0));
				//Add a spike of 2000 nM of sp at time 500;
				sp.inputs.add(new PulseInput(500,2000.0));
	
				double[][] timeSeries2 = os.calculateTimeSeries(null);
				
				double[][] timeSeries = new double[2][];
				timeSeries[0] = timeSeries1[0];
				timeSeries[1] = timeSeries2[0];
				
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i<timeSeries[0].length; i++) {

					for(int j = 0; j<timeSeries.length; j++) {
						sb.append(timeSeries[j][i]);
						sb.append(" ");
					}
					sb.append("\n");
				}

				System.out.println(sb.toString());
				//Displaying time series
				JPanel panel = PlotExpData.displayData("", timeSeries2, os.getActivity(), g);
				JFrame frame = new JFrame("");

				frame.add(panel);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.pack();
				frame.setVisible(true);

	}

}
